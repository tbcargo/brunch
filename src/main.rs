use cursive::{
    Cursive,
    align::HAlign,
    event::{EventResult, Key},
    traits::*,
    view::{scroll::Scroller, Scrollable},
    views::{Dialog, OnEventView, LinearLayout, TextView}
};
use std::{io, env, fmt, fs, process};
use std::io::prelude::*;
use std::cmp::Ordering;
use std::path::PathBuf;
use std::fs::File;

use cursive_tree_view::{Placement, TreeView};

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Please give a filename");
        process::exit(1);
    }
    let filename = &args[1];
    let mut content = String::new();
    let file = File::open(filename);
    match file {
        Ok(mut f) => {
            f.read_to_string(&mut content).expect("Could not read file");
        },
        Err(_) => {
            eprintln!("File not found");
            process::exit(2);
        }
    }

	#[derive(Debug)]
    struct TreeEntry {
        name: String,
        dir: Option<PathBuf>
    }

    impl fmt::Display for TreeEntry {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.name)
        }
    }

    fn collect_entries(dir: &PathBuf, entries: &mut Vec<TreeEntry>) -> io::Result<()> {
        if dir.is_dir() {
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path = entry.path();

                if path.is_dir() {
                    entries.push(TreeEntry {
                        name: entry
                            .file_name()
                            .into_string()
                            .unwrap_or_else(|_| "".to_string()),
                        dir: Some(path)
                    });
                } else if path.is_file() {
                    entries.push(TreeEntry {
                        name: entry
                            .file_name()
                            .into_string()
                            .unwrap_or_else(|_| "".to_string()),
                        dir: None
                    });
                }
            }
        }
        Ok(())
    }

    fn expand_tree(tree: &mut TreeView<TreeEntry>, parent_row: usize, dir: &PathBuf) {
        let mut entries = Vec::new();
        collect_entries(dir, &mut entries).ok();

        entries.sort_by(|a, b| match (a.dir.is_some(), b.dir.is_some()) {
            (true, true) | (false, false) => a.name.cmp(&b.name),
            (true, false) => Ordering::Less,
            (false, true) => Ordering::Greater
        });

        for i in entries {
            if i.dir.is_some() {
                tree.insert_container_item(i, Placement::LastChild, parent_row);
            } else {
                tree.insert_item(i, Placement::LastChild, parent_row);
            }
        }
    }

    let mut tree = TreeView::<TreeEntry>::new();
    let path = env::current_dir().expect("No working directory");

    tree.insert_item(
        TreeEntry {
            name: path.file_name().unwrap().to_str().unwrap().to_string(),
            dir: Some(path.clone())
        },
        Placement::After,
        0
    );

    expand_tree(&mut tree, 0, &path);

    tree.set_on_collapse(|siv: &mut Cursive, row, is_collapsed, children| {
        if !is_collapsed && children == 0 {
            siv.call_on_name("tree", move |tree: &mut TreeView<TreeEntry>| {
                if let Some(dir) = tree.borrow_item(row).unwrap().dir.clone() {
                    expand_tree(tree, row, &dir);
                }
            });
        }
    });

    tree.set_on_submit(|siv: &mut Cursive, row: usize| {
    	let value = siv.call_on_name("tree", move |tree: &mut TreeView<TreeEntry>| {
            tree.borrow_item(row).unwrap().name.to_string()
        });

        let p = env::current_dir().expect("No working directory");

//      siv.add_layer(Dialog::info(format!("{}/{}", fs::canonicalize(&p).unwrap().to_str().unwrap(), value.unwrap())));

        let newfile = File::open(format!("{}/{}", fs::canonicalize(&p).unwrap().to_str().unwrap(), value.unwrap()));
     	match newfile {
     		Ok(mut f) => {
     			let mut newtext = String::new();

     			f.read_to_string(&mut newtext).expect("Could not read file");

     			siv.find_name::<TextView>("fileview").unwrap().set_content(newtext);
     		},
     		Err(_) => {
     			siv.find_name::<TextView>("fileview").unwrap().set_content("Could not open selected file");
     		}
     	}
    });

    tree.set_on_select(|siv: &mut Cursive, row: usize| {    	
    	let value = siv.call_on_name("tree", move |tree: &mut TreeView<TreeEntry>| {
    		tree.borrow_item(row).unwrap().name.to_string()
    	});
    	let mut sub = String::new();
    	let ddir = siv.call_on_name("tree", move |tree: &mut TreeView<TreeEntry>| {
    		tree.borrow_item(row).unwrap().dir.clone()
    	});
    	// sub = ddir.as_path().display().to_string();

    	// this next code will send an info dialog with the path of directory
    	sub = match ddir {
			Some(d) => match d {
				Some(dd) => dd.into_os_string().into_string().unwrap(),
				None => String::from("none")
			},
			None => String::from("none")
		};

    	siv.add_layer(Dialog::info(sub));
    });

    let mut siv = cursive::default();

    siv.add_global_callback('q', |s| s.quit());

    siv.add_fullscreen_layer(
        Dialog::around(LinearLayout::horizontal()
            .child(TextView::new(content)
            	.with_name("fileview")
                .scrollable()
                .wrap_with(OnEventView::new)
                .on_pre_event_inner(Key::PageUp, |v, _| {
                    let scroller = v.get_scroller_mut();
                    if scroller.can_scroll_up() {
                        scroller.scroll_up(scroller.last_outer_size().y.saturating_sub(1));
                    }
                    Some(EventResult::Consumed(None))
                })
                .on_pre_event_inner(Key::PageDown, |v, _| {
                    let scroller = v.get_scroller_mut();
                    if scroller.can_scroll_down() {
                        scroller.scroll_down(scroller.last_outer_size().y.saturating_sub(1));
                    }
                    Some(EventResult::Consumed(None))
                })
            )
            .child(tree.with_name("tree")
                .scrollable()
                .fixed_width(20))
        )
        .title(filename)
        .h_align(HAlign::Center)
    );
    siv.add_layer(Dialog::info(
        "Try resizing the terminal!\nPress Q to quit"
    ));

    siv.run();

    Ok(())
}
